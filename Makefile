ANSIBLE_MAKEFILE_VERSION:="0.16.0"

.PHONY: install
install:
	pip install -r requirements.txt
	command -v ansible-make >/dev/null || wget -O /tmp/ansible-make-$(ANSIBLE_MAKEFILE_VERSION)_amd64.deb \
	  "https://github.com/paulRbr/ansible-makefile/releases/download/$(ANSIBLE_MAKEFILE_VERSION)/ansible-make_$(ANSIBLE_MAKEFILE_VERSION)_amd64.deb"
	command -v ansible-make >/dev/null || dpkg -i /tmp/ansible-make-$(ANSIBLE_MAKEFILE_VERSION)_amd64.deb
	command -v ansible-make >/dev/null || rm /tmp/ansible-make-$(ANSIBLE_MAKEFILE_VERSION)_amd64.deb
	ansible-make install roles_path=vendor/

.PHONY: dry-run-system-config
dry-run-system-config:
	@echo "→ Go for a dry-run playbook"
	@[ "$(limit)" = "" ] && { \
	  echo "→ Please don't use this target without a limit= variable." \
	  && exit 1; \
	} || { \
	  echo "→ limiting to $(limit)" && exit; \
	}
	@[ "$(env)" = "" ] && { \
	  echo "→ Please don't use this target without a env= variable." \
	  && exit 1; \
	} || { \
	  echo "→ Target env is $(env)" && exit; \
	}
	SCW_ACCESS_KEY=$(shell pass terraform/scaleway/$(env)/access_key) \
	SCW_OAUTH_TOKEN=$(shell pass terraform/scaleway/$(env)/secret) \
	SCW_TOKEN=$(shell pass terraform/scaleway/$(env)/token) \
	  ansible-make dry-run

.PHONY: run-system-config
run-system-config:
	@echo "→ Go for a run playbook"
	@[ "$(limit)" = "" ] && { \
	  echo "→ Please don't use this target without a limit= variable." \
	  && exit 1; \
	} || { \
	  echo "→ limiting to $(limit)" && exit; \
	}
	SCW_ACCESS_KEY=$(shell pass terraform/scaleway/$(env)/access_key) \
	SCW_OAUTH_TOKEN=$(shell pass terraform/scaleway/$(env)/secret) \
	SCW_TOKEN=$(shell pass terraform/scaleway/$(env)/token) \
	  ansible-make run env=lgds-prod

.PHONY: dry-run-metabase
dry-run-metabase:
	$(MAKE) dry-run-system-config limit=metabase

.PHONY: run-metabase
run-metabase:
	$(MAKE) run-system-config limit=metabase

plan-resources:
	tf-make plan provider=scaleway env=lgds-prod

apply-resources:
	tf-make apply provider=scaleway env=lgds-prod
