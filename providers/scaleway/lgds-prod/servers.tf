# Create a security group
resource "scaleway_instance_security_group" "web" {
  inbound_default_policy  = "drop"
  outbound_default_policy = "accept"

  # Allow any IP to SSH
  # Mostly because we launch application deployments from Gitlab.com
  # and Gitlab.com doesn't offer stable outgoing IPs for gitlab-runners.
  inbound_rule {
    action = "accept"
    port   = "22"
  }

  # Allow HTTP/HTTPS to serve a user web interface for data analysis
  inbound_rule {
    action = "accept"
    port   = "80"
  }

  inbound_rule {
    action = "accept"
    port   = "443"
  }
}

# Create a public IP
resource "scaleway_instance_ip" "public_ip" {
  count = local.web_prod_server
}

resource "scaleway_instance_server" "web" {
  count = local.web_prod_server
  type  = "DEV1-M"
  image = "d1bab5ca-de30-4840-bba5-d5e0e9316e32" // Backup from database-prod
  tags  = ["lgds", "web", "bdd", "postgresql", "prod", "debian"]

  ip_id             = scaleway_instance_ip.public_ip[count.index].id
  security_group_id = scaleway_instance_security_group.web.id

  lifecycle {
    ignore_changes = [image, tags]
  }
}
