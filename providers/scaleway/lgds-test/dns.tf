provider "ovh" {
  endpoint = "ovh-eu"
}

locals {
  domain = "polr.fr"
}

# Add a record to a sub-domain
resource "ovh_domain_zone_record" "admin-polr-fr" {
  count     = 0
  zone      = local.domain
  subdomain = "admin"
  fieldtype = "A"
  ttl       = "60" # Set to 1m for dev purposes, keep ttl at 1h (3600) when prod
  target    = scaleway_instance_ip.public_ip[count.index].address
}
